﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshVerticeHandler : MonoBehaviour
{
	public GameObject player;
	public BlendableBackgroundGenerator bg_generator;
	MeshBlendProcessor[] blend_target_objects;

	public float blend_start_offset_from_player = 20f;
	public float blend_width = 10f;

	public float blend_start_world_x
	{
		get { return player.transform.position.x + blend_start_offset_from_player; }
	}
	public float blend_end_world_x
	{
		get { return blend_start_world_x + blend_width; }
	}

	public static MeshVerticeHandler ins;
	void Awake()
	{
		if (ins == null) ins = this;
		else Destroy(this);
	}

	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			refresh_vertex_color();
		}
	}

	public void refresh_vertex_color()
	{
		if(blend_target_objects == null)
		{
			blend_target_objects = bg_generator.bg_objects;
		}

		for(int i = 0; i < blend_target_objects.Length; i++)
		{
			blend_target_objects[i].set_blend_amount(blend_start_world_x, blend_end_world_x);
		}
	}
}
