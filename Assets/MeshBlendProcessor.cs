﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBlendProcessor : MonoBehaviour
{
	MeshFilter my_mesh_filter;
	Mesh my_mesh;
	Vector3[] my_vertices;

	// Use this for initialization
	void Start()
	{
		my_mesh_filter = GetComponent<MeshFilter>();
		my_mesh = my_mesh_filter.mesh;
		my_vertices = my_mesh.vertices;
	}

	public void set_blend_amount(float blend_start_world_x, float blend_end_world_x)
	{
		Color[] colors = get_vertex_colors(blend_start_world_x, blend_end_world_x);
		my_mesh.colors = colors;
	}

	Color[] get_vertex_colors(float blend_start_world_x, float blend_end_world_x)
	{
		// create new colors array where the colors will be created.
		Color[] colors = new Color[my_vertices.Length];
		for (int i = 0; i < my_vertices.Length; i++)
		{
			Vector3 vertex_world_pos = MeshBlendUtils.GetVertexWorldPosition(my_vertices[i], my_mesh_filter.gameObject);
			float r = MeshBlendUtils.vertex_blend_amount(vertex_world_pos, blend_start_world_x, blend_end_world_x);
			colors[i] = new Color(r, 0, 0);
		}
		return colors;
	}
}
