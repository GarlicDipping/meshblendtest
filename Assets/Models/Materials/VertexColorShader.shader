﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D


// Unlit shader.
// - no lighting
// - lightmap_kj support
// - vertex color

Shader "Custom/vertex_color_shader" {

	Properties
	{
		_MainTex("Base", 2D) = "white" {}
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" }

		Pass
		{
			CGPROGRAM
			#include "UnityCG.cginc"
			#pragma vertex vert
			#pragma fragment frag
			struct v2f
			{
				half4 color : COLOR;
				fixed4 pos : SV_POSITION;
				fixed2 uv[2] : TEXCOORD0;
			};

			sampler2D _MainTex;
			fixed4 _MainTex_ST;
			v2f vert(appdata_full v)
			{

				v2f o;
				UNITY_INITIALIZE_OUTPUT(v2f, o); // HLSL compiler needs it on Unity 5

				o.pos = UnityObjectToClipPos(v.vertex);

				o.color = v.color;
				return o;
			}

			fixed4 frag(v2f i) : COLOR
			{
				fixed4 c = tex2D(_MainTex, i.uv[0]) * i.color;
				return c;
			}
			ENDCG
		}
	}




}