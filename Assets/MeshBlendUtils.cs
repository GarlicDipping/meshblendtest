﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBlendUtils
{
	public static Vector3 GetVertexWorldPosition(Vector3 vertex, GameObject owner)
	{
		return GetVertexWorldPosition(vertex, owner.transform);
	}

	public static Vector3 GetVertexWorldPosition(Vector3 vertex, Transform owner)
	{
		return owner.localToWorldMatrix.MultiplyPoint3x4(vertex);
	}

	/// <summary>
	/// A에서 B지역으로 이동할때 배경을 얼마만큼 blend할 것인가?
	/// </summary>
	/// <param name="vertex_world_pos"></param>
	/// <returns>A지역에 속하는 vertex일수록 1, B지역에 속하는 vertex일수록 0에 가까움</returns>
	public static float vertex_blend_amount(Vector3 vertex_world_pos, float blend_start_world_x, float blend_end_world_x)
	{
		float blend_width = blend_end_world_x - blend_start_world_x;
		float percentage = Mathf.Clamp01((vertex_world_pos.x - blend_start_world_x) / blend_width);
		float inverted_percentage = 1 - percentage;
		return inverted_percentage;
	}

	public static bool vertex_inside_blend_range(Vector3 world_vertex_pos, float blend_start_world_x, float blend_end_world_x)
	{
		return world_vertex_pos.x <= blend_end_world_x && world_vertex_pos.x >= blend_start_world_x;
	}
}
