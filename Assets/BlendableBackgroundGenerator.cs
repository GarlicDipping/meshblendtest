﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendableBackgroundGenerator : MonoBehaviour
{
	public GameObject main_background_object_prefab;
	public MeshBlendProcessor[] bg_objects { get; private set; }
	public int generate_count = 3;
	Mesh shared_mesh;

	// Use this for initialization
	void Start ()
	{
		shared_mesh = main_background_object_prefab.GetComponent<MeshFilter>().sharedMesh;
		generate_background();	
	}
	
	void Update()
	{
		get_mesh_edges();
	}
	
	void generate_background()
	{
		Vector3 bound_size = mesh_bounds_size();
		Debug.Log("mesh size x : " + bound_size.x + ", mesh size y : " + bound_size.y + ", mesh size z : " + bound_size.z);
		float width = bound_size.x;
		float leftmost_bg_object_pos_x = 0;
		float iterate_count = generate_count / 2;
		
		for(int i = 0; i < iterate_count; i++)
		{
			leftmost_bg_object_pos_x -= width; 
		}
		if(generate_count % 2 == 0)
		{
			leftmost_bg_object_pos_x += width / 2f;
		}

		bg_objects = new MeshBlendProcessor[generate_count];
		for (int i = 0; i < generate_count; i++)
		{
			GameObject current_obj = Instantiate(main_background_object_prefab, 
				new Vector3(leftmost_bg_object_pos_x + i * width, main_background_object_prefab.transform.position.y, main_background_object_prefab.transform.position.z), 
				main_background_object_prefab.transform.rotation);
			current_obj.transform.parent = transform;

			bg_objects[i] = current_obj.GetComponent<MeshBlendProcessor>();
		}
	}

	Vector3 mesh_bounds_size()
	{
		return shared_mesh.bounds.size;
	}

	List<MeshEdgeHelper.Edge> get_mesh_edges()
	{
		List<MeshEdgeHelper.Edge> edges = MeshEdgeHelper.GetEdges(shared_mesh.triangles).FindBoundary().SortEdges();
		foreach(MeshEdgeHelper.Edge e in edges)
		{
			Vector3 start = MeshBlendUtils.GetVertexWorldPosition(shared_mesh.vertices[e.v1], main_background_object_prefab);
			Vector3 end = MeshBlendUtils.GetVertexWorldPosition(shared_mesh.vertices[e.v2], main_background_object_prefab);
			Debug.DrawLine(start, end, Color.red);
		}
		return edges;
	}
}
